using Microsoft.AspNetCore.Mvc;

namespace Senai.Projetos.Checkpoint.V02.Controllers
{
    public class PagesController : Controller
    {


        [HttpGet]
        public IActionResult Sobre()
        {
            return View();
        }

        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public IActionResult Suporte()
        {
            return View();
        }

        [HttpGet]
        public IActionResult Contato()
        {
            return View();
        }
        
        
    }
}