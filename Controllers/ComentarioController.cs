using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Senai.Projetos.Checkpoint.V02.Repositorios;
using Senai.Projetos.Checkpoint.V02.Models;


namespace Senai.Projetos.Checkpoint.V02.Controllers {
    public class ComentarioController : Controller {

        public UsuarioRepositorio UsuarioRepositorio { get; private set; }

        public ComentarioController()
        {
            UsuarioRepositorio = new UsuarioRepositorio();
        }

        [HttpGet]
        public IActionResult Cadastrar () {
            return View ();
        }

        [HttpPost]
        public IActionResult Cadastrar (IFormCollection form) {

            //Buscando o id da session
            int? id = HttpContext.Session.GetInt32("idUsuario");

            //Busca o usuário pelo id
            UsuarioModel usuario = UsuarioRepositorio.BuscarPorId(id.Value);          
            
            ComentarioModel comentario = new ComentarioModel (
                usuario: usuario,
                texto : form["depoimento"]
            );

            ComentarioRepositorio comentarioRepositorio = new ComentarioRepositorio ();
            comentarioRepositorio.Cadastrar (comentario);

            //Falta um feedback pro usuario aqui
            return RedirectToAction ("DashboardUsuario");
        }

    }
}