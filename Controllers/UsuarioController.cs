using System;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.IO;
using Senai.Projetos.Checkpoint.V02.Repositorios;
using Senai.Projetos.Checkpoint.V02.Models;

namespace Senai.Projetos.Checkpoint.V02.Controllers
{
    public class UsuarioController : Controller
    {
        // Criando Get para Cadastrar()
       [HttpGet]
       public IActionResult Cadastrar() {
           return View();
       } 

        //Criando metodo post para Cadastrar()
        [HttpPost]
        public IActionResult Cadastrar(IFormCollection form) {
            UsuarioModel usuario = new UsuarioModel (
                nome: form["nome"],
                email: form["email"],
                senha: form["senha"],
                confirmaSenha: form["confirmaSenha"]
            );

            UsuarioRepositorio usuarioRepositorio = new UsuarioRepositorio();
            usuarioRepositorio.Cadastrar(usuario);

            return RedirectToAction("DashboardUsuario");

        }
        

        // Criando Get para Login()
        [HttpGet]
        public IActionResult Login() {
            return View();
        }

        //Criando metodo post para Login()
        [HttpPost]
        public IActionResult Login(IFormCollection form) {
            UsuarioRepositorio usuarioRepositorio = new UsuarioRepositorio();
            UsuarioModel usuario = usuarioRepositorio.Login(form["email"], form["senha"]);

            if(usuario != null) {
                
                if (usuario.Permissao == false) {
                    HttpContext.Session.SetString("idUsuario", usuario.Id.ToString());
                    return RedirectToAction ("DashboardUsuario");
                } 
                else {
                    return RedirectToAction ("DashboardAdmin");
                }
            }

            return View();


            
        }
    }
}