using System;

namespace Senai.Projetos.Checkpoint.V02.Models
{
    public class ComentarioModel
    {
        public UsuarioModel Usuario { get; set; }
        public int Id { get; set; }
        public string Texto { get; set; }
        public DateTime DataCriacao { get; set; }
        public bool Aprovacao { get; set; }

        public ComentarioModel(UsuarioModel usuario, string texto) {
            this.Usuario = usuario;
            this.Texto = texto;
        }  
           
    

    }
}