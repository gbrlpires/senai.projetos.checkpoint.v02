using System;
using Microsoft.AspNetCore.Mvc;

namespace Senai.Projetos.Checkpoint.V02.Models
{
    public class UsuarioModel
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public string Email { get; set; }
        public string Senha { get; set; }
        public string ConfirmaSenha { get; set; }
        public bool Permissao { get; set; } = false;

        

        /// <summary>
        /// 
        /// </summary>
        /// <param name="nome"></param>
        /// <param name="email"></param>
        /// <param name="senha"></param>
        /// <param name="permissao"></param>
        public UsuarioModel(string nome, string email, string senha, string confirmaSenha) 
        {
            this.Nome = nome;
            this.Email = email;
            this.Senha = senha;
            this.ConfirmaSenha = confirmaSenha;
        }

        public UsuarioModel(int id, string nome, string email, string senha, bool permissao) {
            this.Id = id;
            this.Nome = nome;
            this.Email = email;
            this.Senha = senha;
            this.Permissao = permissao;
        }

    }
}