using Senai.Projetos.Checkpoint.V02.Models;

namespace Senai.Projetos.Checkpoint.V02.Interfaces
{
    public interface IUsuario
    {
        /// <summary>
        /// Cadastra um novo usuário no sistema
        /// </summary>
        /// <param name="usuario"></param>
        /// <returns></returns>
        UsuarioModel Cadastrar(UsuarioModel usuario);

        /// <summary>
        /// Login do usuario no sistema
        /// </summary>
        /// <param name="email"></param>
        /// <param name="senha"></param>
        /// <returns></returns>
        UsuarioModel Login(string email, string senha);

        
        UsuarioModel BuscarPorId(int id);

    }
}