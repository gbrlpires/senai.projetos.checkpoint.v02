using Senai.Projetos.Checkpoint.V02.Models;

namespace Senai.Projetos.Checkpoint.V02.Interfaces
{
    public interface IComentario
    {
        /// <summary>
        /// Cadastrar Comentario no sistema
        /// </summary>
        /// <param name="comentario"></param>
        /// <returns></returns>
         ComentarioModel Cadastrar(ComentarioModel comentario);




         
    }
}