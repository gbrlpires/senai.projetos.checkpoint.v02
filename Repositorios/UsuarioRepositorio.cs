using System.IO;
using Senai.Projetos.Checkpoint.V02.Interfaces;
using Senai.Projetos.Checkpoint.V02.Models;

namespace Senai.Projetos.Checkpoint.V02.Repositorios
{
    public class UsuarioRepositorio : IUsuario
    {
        public UsuarioModel BuscarPorId(int id)
        {
            string[] linhas = System.IO.File.ReadAllLines("usuarios.csv");

            foreach (var item in linhas)
            {
                string[] dados = item.Split(';');

                if (id.ToString() == dados[0]){
                    UsuarioModel usuario = new UsuarioModel(
                                            id: int.Parse(dados[0]),
                                            nome: dados[1],
                                            email: dados[2],
                                            senha: dados[3],
                                            permissao: bool.Parse(dados[4])
                                        );

                    return usuario;
                }
            }

            return null;
        }





        public UsuarioModel Cadastrar(UsuarioModel usuario)
        {
            if (File.Exists("usuarios.csv")) {
                usuario.Id = File.ReadAllLines("usuarios.csv").Length + 1;
            } else {
                usuario.Id = 1;
            }

            using (StreamWriter sw = new StreamWriter ("usuarios.csv", true)) {
               sw.WriteLine ($"{usuario.Id};{usuario.Nome};{usuario.Email};{usuario.Senha};{usuario.Permissao}");
            } 

            return usuario;
        }

        public UsuarioModel Login(string email, string senha) {
            using (StreamReader sr = new StreamReader("usuarios.csv")) {
                while(!sr.EndOfStream) {
                    var linha = sr.ReadLine();

                    if (string.IsNullOrEmpty(linha))
                    {
                        continue;
                    }

                    string[] dados = linha.Split(";");
                    if (dados[2] == email && dados[3] == senha) {
                        UsuarioModel usuario = new UsuarioModel(
                            id: int.Parse (dados[0]),
                            nome: dados[1],
                            email: dados[2],
                            senha: dados[3],
                            permissao: bool.Parse(dados[4])
                        );

                    return usuario;
                    }
                }
            }
        return null;
        }
    }
}