using System.IO;
using Senai.Projetos.Checkpoint.V02.Interfaces;
using Senai.Projetos.Checkpoint.V02.Models;

namespace Senai.Projetos.Checkpoint.V02.Repositorios
{
    public class ComentarioRepositorio : IComentario
    {
        public ComentarioModel Cadastrar(ComentarioModel comentario)
        {
            if (File.Exists("comentarios.csv")) {
                comentario.Id = File.ReadAllLines("comentarios.csv").Length + 1;
            } else {
                comentario.Id = 1;
            }

            using (StreamWriter sw = new StreamWriter("comentarios.csv", true)) {
                // Falta ter os dados do Usuario aqui tb
                sw.WriteLine($"{comentario.Texto}");
            }

            return comentario;
        }
    }
}